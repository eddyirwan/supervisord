import supervisor.xmlrpc
import xmlrpclib,json
from time import sleep
import logging


#http://stackoverflow.com/questions/11743378/talking-to-supervisord-over-xmlrpc

class supervisor_backdoor:

    def __init__(self):
        self.server = xmlrpclib.ServerProxy('http://127.0.0.1',
        transport=supervisor.xmlrpc.SupervisorTransport(
            None,
            None,
            'unix:///var/run/supervisor.sock'
            )
        )

    def _getCurrentStateOfProcessSupervisor(self,scriptsToCheckStatus=[]):
        returnList=[]
        dictjson={}
        for scriptToCheckStatus in scriptsToCheckStatus:
            dictjson={}
            x=self.server.supervisor.getProcessInfo(scriptToCheckStatus)
            dictjson[x['name']]=x['statename']
            returnList.append(dictjson)
        return returnList


    def _stopProcessSupervisor(self,scriptToStopAsArray=[]):
        for x in scriptToStopAsArray:
            try:
                self.server.supervisor.stopProcess(x)
            except Exception as e:
                logging.error("Error Stopping %s: %s"%(x,e))
        return self

    def _startTrainingProcess(self,scriptToTrain="train_model.py"):
        self.server.supervisor.startProcess(scriptToTrain)
        while 1:
            x=self.server.supervisor.getProcessInfo(scriptToTrain)
            if x['statename']=='EXITED':
                logging.info('DONE %s'%(scriptToTrain))
                break
            elif x['statename'] == 'FATAL':
                raise SystemError(process +' IN FATAL MODE')
            elif x['statename'] == 'STOPPED':
                raise SystemError(process +' IN STOPPED MODE')
            else:
                logging.info("Monitoring Process %s: %s"%(scriptToTrain,x['statename']))
            sleep(10)
        return self

    def _startProcessSupervisor(self,scriptToStartAsArray=[]):
        for x in scriptToStartAsArray:
            try:
                self.server.supervisor.startProcess(x)
            except Exception as e:
                logging.error("Error Starting %s: %s" % (x,e))
        return self
