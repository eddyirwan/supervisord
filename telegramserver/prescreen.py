# -*- coding: utf-8 -*-
import settings
import telegram
from time import time
from json import dumps
from telegram.error import TelegramError
import MySQLdb

con = MySQLdb.connect(host=settings.HOST, port=settings.PORT, user=settings.USERNAME, \
                      passwd=settings.PASSWORD, db=settings.DATABASE)
con.set_character_set('utf8')
cursor = con.cursor()

def getthelist(cursor):
    strsql = "SELECT * FROM telegram_fatal where updated_at IS NULL"
    cursor.execute(strsql)
    row = cursor.fetchall()
    return row

def updatethelist(cursor,db,results):
    for row in results:
        apps_id = row[0]
        sql = "UPDATE telegram_fatal SET updated_at = NOW() WHERE id = '%s'" % (apps_id)
        cursor.execute(sql)
        db.commit()

def format_latest_report(results):
    title = "<b>NOTIFICATION SUPERVISOR REPORT</b>\n"
    content = "<pre>FATAL FOUND.Please do something about it</pre>\n"
    message = title + "\n" + content
    tail=''
    for row in results:
        apps_id = row[0]
        apps_name = row[1]
        created_at = row[2]
        tail="%s %s\n"%(tail,apps_name)
    return message + tail

bot = telegram.Bot(token=settings.TOKEN_PRESCREEN)
senaraiUtkDihantar=getthelist(cursor)
msg_1 = format_latest_report(senaraiUtkDihantar)
updatethelist(cursor,con,senaraiUtkDihantar)
parse_mode='HTML'


con.close()
if len(senaraiUtkDihantar) > 0:
    try:
        try:
            chat_id = bot.getUpdates()[-1].message.chat_id
        except:
            chat_id = settings.chat_id_tw_prescreen

        bot.sendMessage(chat_id=chat_id, text=msg_1, parse_mode=parse_mode)
    except TelegramError, err:
        print 'telegram errors'
