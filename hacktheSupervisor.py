#!/usr/bin/python

from lib.supervisor_backdoor import supervisor_backdoor
import os,shutil,glob,os
import supervisor.xmlrpc
import xmlrpclib,json,logging
from datetime import datetime,timedelta
from time import sleep
import argparse
import settings



#################################################################################
# PROGRMAMER WILL                                                            #
#                                                                               #
# The only purpose to create this script is to stop new incoming message to the #
# sentiment until snapshot being taken by system.                               #
#################################################################################
class redirectIssueTo:
	@staticmethod
	def send2mysqlasfatal(apps_name,cursor,db):
		sql = "INSERT INTO telegram_fatal (apps_name,created_at, updated_at) VALUES ('%s', NOW(), NULL)"%(apps_name)
		cursor.execute(sql)
		db.commit()

class hackTheSupervisor(supervisor_backdoor):
	def __init__(self,listOfSupervisorService=[]):
		self.listOfSupervisorService=listOfSupervisorService
		supervisor_backdoor.__init__(self)

	def stop(self):
		self._stopProcessSupervisor(self.listOfSupervisorService)

	def start(self):
		self._startProcessSupervisor(self.listOfSupervisorService)

	def check(self):
		con, cursor = settings.db.init_mysql_production()
		for dict in self._getCurrentStateOfProcessSupervisor(self.listOfSupervisorService):
			for key, value in dict.iteritems():
				if value == "FATAL":
					redirectIssueTo.send2mysqlasfatal(key,cursor,con)
				print '%s->%s'%(key,value)
		con.close()



if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Start and Stop supervisor service')
	parser.add_argument('-t','--task', help='start / stop / check', required=True)
	args = vars(parser.parse_args())
	abc = hackTheSupervisor([
		'api_d2v_daily_custom',
		'api_d2v_daily_custom_bn',
		'api_d2v_daily_custom_najib',
		'api_d2v_daily_custom_oppo',
		'api_hardcode_leaders'
		])

	try:
		xxx = getattr(abc, args['task'])
	except AttributeError:
		print "function not found %s"%args['task']
	else:
		xxx()



#################
# Yours Sincerely
#################
# Programmer Sesuap Nasik
#################################
